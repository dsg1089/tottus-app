const jwt = require('jsonwebtoken');

let authorized = (req, res, next) => {

    let token = req.get('token');
    
    jwt.verify(token, process.env.SEED, (err, decoded) => {

        if (err) {
            return res.status(401).json({error:  'Su sesión ha expirado, por favor acceda nuevamente'});
        }
        req.usuario = decoded.usuario;
        next()
    });
};

let boss_user = (req, res, next) => {

    let usuario = req.usuario;

    if (usuario.cargo == 'jefe') {
        next();
    } else {
        return res.status(401).json({error:  'Operación exclusiva de usuario jefe'});
    }
};


module.exports = { authorized, boss_user }