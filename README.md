# Tottus App

CRUD de ordenes de reposicion de los productos registrados, con rutas documentadas con swagger y usando jwt para la autorización de las solicitudes

## Instalacion

Clonar este proyecto:
```bash
git clone https://gitlab.com/dsg1089/tottus-app.git
```

Instalar mysql y crear la base de datos de acuerdo al archivo de ayuda `database.sql` dentro de la carpeta `config`

Instalar librerías
```
npm install
```

Levantar servidor local
```
npm run dev
```

Visitar la ruta `/api-docs` para ver la UI de Swagger


