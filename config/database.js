const mysql = require('mysql');

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '12345', // poner su contraseña personal
    database: 'tottus'
})

connection.connect((error)=>{
  if(error) return console.log('No se pudo conectar a la base de datos')
  console.log('Conectado a la base de datos')
})

module.exports = connection
