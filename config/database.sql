-- ejecutar este query en Mysql Command Line

CREATE DATABASE tottus;

USE DATABASE tottus;

CREATE TABLE Usuarios(
    id VARCHAR(8) NOT NULL PRIMARY KEY,
    pass VARCHAR(255) NOT NULL,
    nombre VARCHAR(255) NOT NULL,
    cargo VARCHAR(255) NOT NULL
);

CREATE TABLE Productos(
    id INT PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(255) NOT NULL,
    categoria VARCHAR(255) NOT NULL
);

CREATE TABLE Ordenes(
  id INT AUTO_INCREMENT,
  rl_id VARCHAR(8),
  producto_id INT NOT NULL,
  cantidad FLOAT NOT NULL,
  finalizado TINYINT(1) DEFAULT 0,
  PRIMARY KEY(id),
  FOREIGN KEY (rl_id) REFERENCES Usuarios(id),
  FOREIGN KEY (producto_id) REFERENCES Productos(id)
);


-- contraseña para todos : 12345 encriptada

INSERT INTO Usuarios(id,pass,nombre,cargo)
VALUES(
        '00000000',
        '$2b$10$6SiZ9tB8xpGqKm8mgIGtzOZZHcc8MnSjInOsE3iLuEicnInnrihIi',
        'Jefe1',
        'jefe'
    ),
    (
        '11111111',
        '$2b$10$6SiZ9tB8xpGqKm8mgIGtzOZZHcc8MnSjInOsE3iLuEicnInnrihIi',
        'Reponedor 1',
        'RL'
    ),
    (
        '22222222',
        '$2b$10$6SiZ9tB8xpGqKm8mgIGtzOZZHcc8MnSjInOsE3iLuEicnInnrihIi',
        'Reponedor 2',
        'RL'
    );