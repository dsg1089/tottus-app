const express = require('express');
const connection = require('../config/database');

const {authorized, boss_user} = require('../middlewares/autentication');

const app = express();

/**
 * @swagger
 * /productos:
 *    get:
 *      summary: Ver lista de productos registrados
 *      tags: [Producto]
 */
app.get('/productos',[authorized],(req, res)=>{

    connection.query('SELECT * FROM Productos',(err,productos)=>{
        return res.json(productos)
    })

});


/**
 * @swagger
 * /producto:
 *    post:
 *      summary: Registrar un nuevo producto
 *      tags: [Producto]
 *    parameters:
 *      - in: body
 *        name: producto
 *        schema:
 *          type: object
 *          properties:
 *            nombre:
 *              type: string
 *              example: Panetón TOTTUS
 *              description: Nombre del producto
 *            categoria:
 *              type: string
 *              example: Alimentación
 *              description: Categoría a la que corresponde   
 */
app.post('/producto',[authorized],(req, res)=>{

    let {nombre,categoria} = req.body

    if(!nombre || !categoria) return res.status(400).json({error: 'Debe enviar tanto nombre como categoría'})

    let nuevoProducto = `
        INSERT INTO Productos(nombre,categoria)
        VALUES(
            '${nombre}',
            '${categoria}'
        );
    `

    connection.query(nuevoProducto,(err,productoDB)=>{
        if(err) res.status(500).json({error: 'No se pudo agregar producto',details:err})

        return res.json({
            producto:{
                id: productoDB.insertId,
                nombre,
                categoria
            }
        })
    })

});


module.exports = app;