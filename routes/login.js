const express = require('express');
const connection = require('../config/database');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const app = express();


/**
 * @swagger
 * /login:
 *    post:
 *      summary: Iniciar sesión para obtener token
 *      description: Se deberá enviar el token recibido en cada cabecera de las próximas solicitudes(el token expira en 24h)
 *      tags: [Login]
 *    parameters:
 *      - in: body
 *        name: usuario
 *        schema:
 *          type: object
 *          properties:
 *            id:
 *              type: string
 *              example: 11111111
 *              description: Id del usuario(DNI)
 *            password:
 *              type: string
 *              example: 12345
 *              description: Contraseña del usuario   
 */
app.post('/login', async (req, res) => {

    let {id, password} = req.body

    connection.query(`SELECT * FROM Usuarios WHERE id = '${id}' LIMIT 1`,(err,result)=>{

        let usuario = JSON.parse(JSON.stringify(result))[0]

        if(!usuario){
            return res.status(400).json({error:'usuario no encontrado'})
        }

        if(!bcrypt.compareSync(password, usuario.pass)) {
            return res.status(400).json({error:'contraseña inválida'})
        }

        delete usuario.pass

        let token = jwt.sign({usuario}, process.env.SEED, { expiresIn: '1d'});

        res.json({
            usuario,
            token
        });

    })


});

module.exports = app;