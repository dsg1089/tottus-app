const express = require('express');
const connection = require('../config/database');

const {authorized, boss_user} = require('../middlewares/autentication');

const app = express();

/**
 * @swagger
 * /usuarios:
 *    get:
 *      summary: Ver lista de RL's registrados
 *      description: Solo disponible para usuario de tipo 'jefe'
 *      tags: [Usuario]
 */
app.get('/usuarios',[authorized,boss_user],(req, res)=>{
    let query = `
        SELECT id,nombre
        FROM Usuarios
        WHERE cargo = 'RL'
    `
    connection.query(query,(err,usuarios)=>{
        return res.json(usuarios)
    })
});


module.exports = app;