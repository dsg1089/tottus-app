const express = require('express');
const connection = require('../config/database');

const {authorized, boss_user} = require('../middlewares/autentication');

const app = express();


/**
 * @swagger
 * /ordenes/todo:
 *    get:
 *      summary: Ver lista completa de órdenes
 *      description: Solo disponible para usuario de tipo 'jefe'
 *      tags: [Orden]
 */
app.get('/ordenes/todo',[authorized,boss_user],(req, res)=>{

    console.log('ss')
    connection.query('SELECT * FROM Ordenes',(err,productos)=>{
        return res.json(productos)
    })

});


/**
 * @swagger
 * /ordenes/asignadas:
 *    get:
 *      summary: Ver lista de órdenes asignadas
 *      description: Solo se mostrarán la lista de ordenes asignadas que tiene el usuario según el token enviado
 *      tags: [Orden]
 */
app.get('/ordenes/asignadas',[authorized],(req, res)=>{

    connection.query(`SELECT * FROM Ordenes WHERE rl_id=${req.usuario.id}`,(err,ordenes)=>{
        return res.json(ordenes)
    })

});


/**
 * @swagger
 * /orden:
 *    post:
 *      summary: Registrar nueva orden de reposición
 *      description: Si ya se registró una orden se actualizará con la nueva cantidad solicitada mientras la orden no haya sido finalizada
 *      tags: [Orden]
 *    parameters:
 *      - in: body
 *        name: orden
 *        schema:
 *          type: object
 *          properties:
 *            producto_id:
 *              type: integer
 *              example: 1
 *              description: Id del producto seleccionado
 *            cantidad:
 *              type: number
 *              example: 10.5
 *              description: Cantidad solicitada para reponer   
 */
app.post('/orden',[authorized],(req, res)=>{

    let {producto_id,cantidad} = req.body

    if(!producto_id || !cantidad) return res.status(400).json({error: 'Debe enviar tanto id de producto como cantidad'})


    connection.query(`SELECT * FROM Ordenes WHERE producto_id=${producto_id} and finalizado = 0`,(e,ordenExistente)=>{
        let queryOrden = `
            INSERT INTO Ordenes(producto_id,cantidad)
            VALUES(
                '${producto_id}',
                ${cantidad}
            );
        `

        console.log(ordenExistente)
        if(ordenExistente.length>0){
            queryOrden = `
                UPDATE Ordenes
                SET cantidad = ${cantidad}
                WHERE producto_id=${producto_id} 
            `
        }

        connection.query(queryOrden,(err,ordenDB)=>{
            if(err) res.status(500).json({error: 'No se pudo agregar orden',details:err})
            console.log(ordenDB)
            return res.json({
                orden:{
                    id: ordenDB.insertId>0?ordenDB.insertId:ordenExistente[0].id,
                    producto_id,
                    cantidad
                }
            })
        })
    })

});


/**
 * @swagger
 * /orden/asignar:
 *    put:
 *      summary: Asignar una orden a un RL
 *      description: Se le permite a un jefe reasignar órdenes ya asignadas
 *      tags: [Orden]
 *    parameters:
 *      - in: body
 *        name: orden
 *        schema:
 *          type: object
 *          properties:
 *            id:
 *              type: integer
 *              example: 1
 *              description: Id de la orden seleccionada
 *            rl_id:
 *              type: string
 *              example: 11111111
 *              description: id del RL (DNI)   
 */
app.put('/orden/asignar',[authorized,boss_user],(req, res)=>{

    let {id, rl_id} = req.body

    if(!id || !rl_id) return res.status(400).json({error: 'Debe enviar tanto id de orden como id de RL'})
    if(String(rl_id).length!=8) return res.status(400).json({error: 'No es un id de usuario válido'})

    let queryAsignar=`
        UPDATE Ordenes
        SET rl_id = ${rl_id}
        WHERE id = ${id} and finalizado = 0
    `

    connection.query(queryAsignar,(err,ordenAsignada)=>{
        if(err){
            console.log(err)
            return res.status(500).json({error: 'No se pudo asignar orden'})
        }

        if(ordenAsignada.affectedRows == 0) return res.status(400).json({error:'Orden ya ha sido finalizada'})

        return res.json({ok:true})
    })

});



/**
 * @swagger
 * /orden/finalizar:
 *    put:
 *      summary: Marcar como finalizada una orden
 *      description: Solo podrá finalizar una orden que le fue asignada al RL
 *      tags: [Orden]
 *    parameters:
 *      - in: body
 *        name: orden
 *        schema:
 *          type: object
 *          properties:
 *            id:
 *              type: integer
 *              example: 1
 *              description: Id de la orden seleccionada
 */
app.put('/orden/finalizar',[authorized],(req, res)=>{

    let {id} = req.body

    if(!id) return res.status(400).json({error: 'Debe enviar id de orden'})

    let queryFinalizar=`
        UPDATE Ordenes
        SET finalizado = 1
        WHERE id = ${id} and finalizado = 0 and rl_id = ${req.usuario.id}
    `

    connection.query(queryFinalizar,(err,ordenFinalizada)=>{

        if(err){
            console.log(err)
            return res.status(500).json({error: 'No se pudo finalizar orden'})
        }
        if(ordenFinalizada.affectedRows == 0) return res.status(400).json({error:'Orden ya ha sido finalizada o no ha sido asignada a este usuario'})

        return res.json({ok:true})

    })

});



/**
 * @swagger
 * /orden/${id}:
 *    delete:
 *      summary: Eliminar una orden que aún no ha sido finalizada
 *      tags: [Orden]
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        description: id de la orden elegida
 *        type: integer
 *        example: 2
 */
app.delete('/orden/:id',[authorized,boss_user],(req,res)=>{
    connection.query(`DELETE * FROM Ordenes WHERE id=${req.params.id} and finalizado = 0`,(err,ordenBorrada)=>{
        if(err){
            console.log(err)
            return res.status(500).json({error: 'No se pudo borrar orden'})
        }
        if(ordenBorrada.affectedRows == 0) return res.status(400).json({error:'Orden ya ha sido finalizada'})

        return res.json({ok:true})
    })
})


module.exports = app;