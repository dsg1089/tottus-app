require('./config/config');
const express = require('express');
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

const app = express();
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE");
  next();
});


const swaggerOptions = {
  swaggerDefinition: {
    info: {
      version: "1.0.0",
      title: "Tottus App API",
      description: "Documentación de las rutas disponibles en esta API",
      contact: {
        name: "David Silva",
        email: "dsg1089@gmail.com"
      },
      servers: [`http://localhost:${process.env.PORT}`]
    }
  },
  apis: ['./routes/*.js']
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.use(require('./routes/login'));
app.use(require('./routes/usuario'));
app.use(require('./routes/producto'));
app.use(require('./routes/orden'));


app.listen(process.env.PORT,() => {
    console.log(`App funcionando en el puerto ${process.env.PORT}`);
});